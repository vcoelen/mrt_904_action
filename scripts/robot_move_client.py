#! /usr/bin/env python

import rospy
from __future__ import print_function

import actionlib
import mrt_904_action.msg

def robot_move_client():
    client = actionlib.SimpleActionClient('robot_move', mrt_904_action.msg.RobotMoveAction)

    client.wait_for_server()

    # Creates a goal
    goal = mrt_904_action.msg.RobotMoveGoal()
    # ---
    # Here modify the goal to move the robot 1m in x, 0.5m in y
    # ---

    # Sends the goal to the action server.
    client.send_goal(goal, active_cb=move_active_cb, feedback_cb=move_feedback_cb, done_cb=move_done_cb)

    # Waits for the server to finish performing the action.
    client.wait_for_result()

    # Prints out the result of executing the action
    return client.get_result()



def move_active_cb(self):
    self._loginfo('Move Action server goal has transitioned to active state')

def move_feedback_cb(self, feedback):
    self._loginfo('Move action server feedback received')
    # ---
    # Here log the feedback informations

def move_done_cb(self, state, result):
    self._loginfo('Move Action server done callback triggered')
    # ---
    # Here log the state and the Result



if __name__ == '__main__':
    try:
        rospy.init_node('robot_move_client')
        result = robot_move_client()
        # ---
        # Here you can also log the result
    except rospy.ROSInterruptException:
        print("program interrupted before completion", file=sys.stderr)
