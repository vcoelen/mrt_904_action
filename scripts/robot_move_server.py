#! /usr/bin/env python

import rospy

import actionlib

import mrt_904_action.msg


class RobotMoveAction(object):
    # create messages that are used to publish feedback/result
    _feedback = mrt_904_action.msg.RobotMoveFeedback()
    _result = mrt_904_action.msg.RobotMoveResult()

    def __init__(self, name):
        self._action_name = name
        self._as = actionlib.SimpleActionServer(self._action_name, mrt_904_action.msg.RobotMoveAction, execute_cb=self.execute_cb, auto_start = False)
        self._as.start()

    def execute_cb(self, goal):
        r = rospy.Rate(10)
        success = True

        # ---
        # Here initialize feedback and result
        # ---

        #Log info to the user
        rospy.loginfo('%s: Executing' % (self._action_name))

        # start executing the action
        while rospy.ok(): # Here add condition of completion

            # check that preempt has not been requested by the client
            if self._as.is_preempt_requested():
                rospy.loginfo('%s: Preempted' % self._action_name)
                self._as.set_preempted()
                success = False
                break

            # ---
            # Do the actal work
            # to publish a feedback  : self._as.publish_feedback(self._feedback)
            # ---
            r.sleep()


        if success:
            rospy.loginfo('%s: Succeeded' % self._action_name)
            self._as.set_succeeded(self._result)

if __name__ == '__main__':
    rospy.init_node('robot_move_server')
    server = RobotMoveAction('robot_move')
    rospy.spin()
